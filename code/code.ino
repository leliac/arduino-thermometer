//=== PREPROCESSOR DEFINITIONS ===//

// GENERAL
#define VERBOSE // verbose output
#define BAUDRATE 2000000 // serial transmission rate (in bit/s)
#define NMEASUREMENTS 10 // number of measurements (-1 for infinity)
#define DELAY 0 // delay between measurements (in ms)
#define SECOND_THERMOMETER // use voltage divider and internal ADC reference
#define STATISTICAL_APPROACH // perform averaged measurements, gathering samples with an ISR triggered by a hardware timer

// SENSOR
#define K (float)0.01 // sensor sensitivity (in V/K)
#define DELTA_S 1 // sensor accuracy (in °C)

// ADC
#define CHANNEL 0 // analog input
#define NLEVELS 1024 // number of quantization intervals
#define DELTA_N (float)0.5 // ADC accuracy (in LSB)

// STATISTICAL MEASUREMENTS
#ifdef STATISTICAL_APPROACH
  #define NSAMPLES 500 // number of samples per measurement
  #define SAMPLING_WINDOW (float)0.08 // sampling window (in s)
  #define ISR_CHRONOMETER // time ISRs to estimate maximum sampling frequency
  #define NO_ANALOGREAD // directly manipulate ADC registers
#endif

#ifdef SECOND_THERMOMETER

  // INTERNAL ADC REFERENCE
  #define VAREF (float)1.08541 // internal ADC reference (in V)
  #define DELTA_VAREF (float)(0.00004*VAREF+0.000007) // VAREF uncertainty (in V, deterministic)
  
  // VOLTAGE DIVIDER
  #define R1 (float)2674.2 // voltage divider resistance 1 (in Ω)
  #define DELTA_R1_TILDE (float)(0.0001*R1+0.1) // R1 uncertainty at T_R1 (in Ω, deterministic)
  #define T_R1 (float)24.1 // temperature at which R1 is measured (in °C)
  #define R2 (float)986.48 // voltage divider resistance 2 (in Ω)
  #define DELTA_R2_TILDE (float)(0.0001*R2+0.01) // R2 uncertainty at T_R2 (in Ω, deterministic)
  #define T_R2 (float)24.3 // temperature at which R2 is measured (in °C)
  #define ALPHA (float)0.0001 // temperature coefficient of R1 and R2 (in 1/°C)

#else

  // DEFAULT ADC REFERENCE
  #define VAREF (float)5 // default ADC reference (in V)
  #define DELTA_VAREF (float)0.5 // VAREF uncertainty (in V)

#endif

//=== GLOBAL VARIABLES ===//

unsigned long i = 0; // measurement counter
#ifdef STATISTICAL_APPROACH
  unsigned int j = 0; // sample counter
  unsigned int Nj[NSAMPLES]; // ADC output samples
  float u_Nj = DELTA_N / sqrt(3); // Nj's uncertainty (combined standard)
  float u_VAREF = DELTA_VAREF / sqrt(3); // VAREF uncertainty (combined standard)
  float u_S = DELTA_S / sqrt(3); // sensor uncertainty (combined standard)
  #ifdef SECOND_THERMOMETER
    float u_R1_tilde = DELTA_R1_TILDE / sqrt(3); // R1 uncertainty at T_R1 (combined standard)
    float u_R2_tilde = DELTA_R2_TILDE / sqrt(3); // R2 uncertainty at T_R2 (combined standard)
  #endif
  float sampling_freq_requested = NSAMPLES/SAMPLING_WINDOW; // requested sampling frequency
  bool isr_done = false; // ISR complete flag
  #ifdef ISR_CHRONOMETER
    unsigned long isr_dur_max = 0; // maximum ISR duration
  #endif
#else
  float N; // ADC output
  float u_N = DELTA_N; // N uncertainty (deterministic)
  float u_VAREF = DELTA_VAREF; // VAREF uncertainty (deterministic)
  float u_S = DELTA_S; // sensor uncertainty (deterministic)
  #ifdef SECOND_THERMOMETER
    float u_R1_tilde = DELTA_R1_TILDE; // R1 uncertainty at T_R1 (deterministic)
    float u_R2_tilde = DELTA_R2_TILDE; // R2 uncertainty at T_R2 (deterministic)
  #endif
#endif

//=== FUNCTION DEFINITIONS ===//

void setup() {
  Serial.begin(BAUDRATE);
  #ifdef VERBOSE
    printMeasurement("VAREF", "V", VAREF, u_VAREF, 5, 5, 3);
    #ifdef SECOND_THERMOMETER
      Serial.print("At ");
      Serial.print(T_R1, 1);
      Serial.print("°C, ");
      printMeasurement("R1", "Ω", R1, u_R1_tilde, 1, 1, 2);
      Serial.print("At ");
      Serial.print(T_R2, 1);
      Serial.print("°C, ");
      printMeasurement("R2", "Ω", R2, u_R2_tilde, 1, 1, 2);
    #endif
  #endif

  // ADC INITIALIZATION
  #ifdef NO_ANALOGREAD
    #ifdef SECOND_THERMOMETER
      ADMUX = (1 << REFS0) | (1 << REFS1) | CHANNEL; // select internal ADC reference and analog input CHANNEL
    #else
      ADMUX = (1 << REFS0) | CHANNEL; // select default ADC reference and analog input CHANNEL
    #endif
    ADCSRA |= (1 << ADEN); // enable ADC
    ADCSRA |= (1 << ADSC); // trigger ADC
  #else
    #ifdef SECOND_THERMOMETER
      analogReference(INTERNAL);
    #else
      analogReference(DEFAULT);
    #endif
    analogRead(CHANNEL);
  #endif
  delay(1); // wait for circuit to settle

  // ISR INITIALIZATION
  #ifdef STATISTICAL_APPROACH
    float sampling_freq_actual = initTimer1(sampling_freq_requested); // set and get hardware timer frequency
    #ifdef VERBOSE
      Serial.print("Requested sampling frequency: ");
      Serial.print(sampling_freq_requested);
      Serial.println("Hz");
      Serial.print("Actual sampling frequency: ");
      Serial.print(sampling_freq_actual);
      Serial.println("Hz");
    #endif
    enableTimer1Interrupt();
  #endif

  #ifdef VERBOSE
    Serial.println();
  #endif
}

void loop() {
  if (NMEASUREMENTS == -1 || i < NMEASUREMENTS) {

    // STATISTICAL MEASUREMENT
    #ifdef STATISTICAL_APPROACH
      if (isr_done) {
        ++i;
        #ifdef VERBOSE
          Serial.print("Measurement #");
          Serial.println(i);
        #endif
        getTemperature();
        if (i == NMEASUREMENTS) {
          #ifdef ISR_CHRONOMETER
            Serial.print("Maximum sampling frequency: ");
            Serial.print(1000000. / isr_dur_max);
            Serial.println("Hz");
          #endif
        } else {
          delay(DELAY);
          j = 0;
          isr_done = false;
          enableTimer1Interrupt();
        }
      }

    // DETERMINISTIC MEASUREMENT
    #else
      ++i;
      #ifdef VERBOSE
        Serial.print("Measurement #");
        Serial.println(i);
      #endif
      N = analogRead(CHANNEL) + 0.5; // add 0.5 to select center of quantization interval
      getTemperature();
      delay(DELAY);
    #endif
  }
}

#ifdef STATISTICAL_APPROACH

  ISR(TIMER1_COMPA_vect) {
    #ifdef ISR_CHRONOMETER
      unsigned long isr_t0 = micros(); // start ISR chronometer
    #endif
    #ifdef NO_ANALOGREAD
      ADCSRA |= (1 << ADSC); // trigger ADC
      while ((ADCSRA & (1 << ADSC)) != 0); // wait for ADC to finish
      Nj[j++] = ADCL | (ADCH << 8); // get ADC output
    #else
      Nj[j++] = analogRead(CHANNEL);
    #endif
    #ifdef ISR_CHRONOMETER
      unsigned long isr_dur = micros() - isr_t0 + 11; // stop ISR chronometer, add time required to call ISR and perform end check
      if (isr_dur > isr_dur_max) {
        isr_dur_max = isr_dur;
      }
    #endif
    if (j == NSAMPLES) {
      disableTimer1Interrupt();
      isr_done = true;
    }
  }

  void getTemperature(void) {
  
    // AVERAGE ADC OUTPUT
    #ifdef VERBOSE
      Serial.print("Nj = ([ ");
    #endif
    float N = 0; // average ADC output
    for (j = 0; j < NSAMPLES; ++j) {
      #ifdef VERBOSE
        Serial.print(Nj[j] + 0.5, 1); // add 0.5 to select center of quantization interval
        Serial.print(" ");
      #endif
      N += Nj[j] + 0.5; // add 0.5 to select center of quantization interval
    }
    #ifdef VERBOSE
      Serial.print("] +- ");
      Serial.print(u_Nj, 1);
      Serial.println(")LSB");
    #endif
    N /= NSAMPLES;
    float u_N = 0; // N uncertainty (combined standard)
    for (j = 0; j < NSAMPLES; ++j) {
      u_N += pow(Nj[j] + 0.5 - N, 2); // add 0.5 to select center of quantization interval
    }
    u_N = sqrt(pow(u_Nj, 2) + u_N / (NSAMPLES - 1) / NSAMPLES);
    #ifdef VERBOSE
      printMeasurement("N", "LSB", N, u_N, 1, 1, 2);
    #endif
  
    // AVERAGE TEMPERATURE
    #ifdef SECOND_THERMOMETER
      float T = N * VAREF / NLEVELS * (1 + R1 / R2) / K - 273.15; // average temperature
      float u_R1 = u_R1_tilde + R1 * ALPHA * abs(T - T_R1) / sqrt(3); // R1 uncertinaty (combined standard)
      float u_R2 = u_R2_tilde + R2 * ALPHA * abs(T - T_R2) / sqrt(3); // R2 uncertinaty (combined standard)
      float u_T = sqrt((pow(VAREF * (1 + R1 / R2) * u_N, 2) +
                  pow(N * (1 + R1 / R2) * u_VAREF, 2) +
                  pow(N * VAREF / R2 * u_R1, 2) +
                  pow(N * VAREF * R1 * u_R2, 2) / pow(R2, 4) -
                  2 * pow(N * VAREF, 2) * R1 / pow(R2, 3) * u_R1 * u_R2) /
                  pow(NLEVELS, 2) / pow(K, 2) + pow(u_S, 2)); // T uncertainty (combined standard)
    #else
      float T = N * VAREF / NLEVELS / K - 273.15; // average temperature
      float u_T = sqrt((pow(VAREF * u_N, 2) +
                  pow(N * u_VAREF, 2)) /
                  pow(NLEVELS, 2) / pow(K, 2) + pow(u_S, 2)); // T uncertainty (combined standard)
    #endif
    printMeasurement("T", "°C", T, u_T, 1, 1, 0);
    Serial.println();
  }

  float initTimer1(float interrupt_frequency) {
    // this function returns the actual time interval
    // the clock is 16MHz i.e. 62.5 ns
    // the timer can count up to 65535
    // we have these scalers
    int scalers[] = {1, 8, 64, 256, 1024};
    // look for the required scaler
    int scalerVal;
    for (scalerVal = 0; scalerVal < 5; scalerVal++) {
      float minFreq = 1 / (62.5e-9 * scalers[scalerVal] * 65536);
      if (interrupt_frequency > minFreq) {
        break;
      }
    }
    float interval = 1 / interrupt_frequency;
    // now compute the matching code according to the scaler
    unsigned int code = (unsigned int) (interval / (62.5e-9 * scalers[scalerVal]));
    // set the scaler
    TCCR1A = 0;
    TCCR1B = 0;
    setTimer1Prescaler(scalers[scalerVal]);
    // load the match code
    OCR1A = code;
    // set the timer in compare mode
    TCCR1B |= (1 << WGM12); // CTC mode
    // now return the actual freq
    return 1.0 / (code * 62.5e-9 * scalers[scalerVal]);
  }
  
  void setTimer1Prescaler(int prescaler) {
    TCCR1B &= 0xF8; // clear the three lower bits
    switch (prescaler) {
      case 1:
        TCCR1B |= (1 << CS10);
        break;
      case 8:
        TCCR1B |= (1 << CS11);
        break;
      case 64:
        TCCR1B |= (1 << CS11) | (1 << CS10);
        break;
      case 256:
        TCCR1B |= (1 << CS12);
        break;
      case 1024:
        TCCR1B |= (1 << CS12) | (1 << CS10);
        break;
      default: TCCR1B |= (1 << CS12) ;
    }
  }
  
  void enableTimer1Interrupt(void) {
    TCNT1 = 0;
    // clear timer
    // since the timer was already running
    // it reached the match value
    // and an interrupt is pending
    // enabling the mask fires it
    // unless we clear it
    TIFR1 = (1 << OCF1A); // clear match flag
    TIMSK1 = (1 << OCIE1A); // enable only timer compare interrupt
  }
  
  void disableTimer1Interrupt(void) {
    TIMSK1 = 0; // disable interrupt
  }
  
#else

  void getTemperature(void) {
    #ifdef VERBOSE
      printMeasurement("N", "LSB", N, u_N, 1, 1, 2);
    #endif
    float VA = N * VAREF / NLEVELS; // ADC input voltage
    float u_VA = VA * (u_VAREF / VAREF + u_N / N); // VA uncertainty (deterministic)
    #ifdef VERBOSE
      printMeasurement("VA", "V", VA, u_VA, 4, 4, 2);
    #endif
    #ifdef SECOND_THERMOMETER
      float T = VA * (1 + R1 / R2) / K - 273.15; // temperature
      float u_R1 = u_R1_tilde + R1 * ALPHA * abs(T - T_R1); // R1 uncertinaty (deterministic)
      float u_R2 = u_R2_tilde + R2 * ALPHA * abs(T - T_R2); // R2 uncertinaty (deterministic)
      float VPLUS = VA * (1 + R1 / R2); // sensor output voltage
      float u_VPLUS = (1 + R1 / R2) * u_VA +
                VA / R2 * u_R1 +
                VA * R1 / (R2 * R2) * u_R2; // VPLUS uncertainty (deterministic)
      #ifdef VERBOSE
        printMeasurement("VPLUS", "V", VPLUS, u_VPLUS, 3, 3, 1);
      #endif
      float u_T = u_VPLUS / K + u_S; // T uncertainty (deterministic)
    #else
      float T = VA / K - 273.15; // temperature
      float u_T = u_VA / K + u_S; // T uncertainty (deterministic)
    #endif
    printMeasurement("T", "°C", T, u_T, 0, 0, 0);
    Serial.println();
  }
  
#endif

void printMeasurement(const char *symbol, const char *unit, float X, float u_X, int dp_X, int dp_u_X, int dp_ur_X) {
  Serial.print(symbol);
  Serial.print(" = (");
  Serial.print(X, dp_X);
  Serial.print(" +- ");
  Serial.print(u_X, dp_u_X);
  Serial.print(")");
  Serial.print(unit);
  Serial.print(", ");
  Serial.print(100 * u_X / X, dp_ur_X);
  Serial.println("%");
}
