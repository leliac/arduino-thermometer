# Arduino Thermometer
Electronic thermometer with Arduino Uno and LM335. See the [Wiki](https://gitlab.com/leliac/arduino-thermometer/-/wikis/home) for more information.